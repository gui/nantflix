<?php
/* Page privée de séries de l'utilisateur */
	include("includes/base.php"); 					// Base : header et navbar
	include("includes/session.php"); 				// Vérifie que l'utilisateur est connecté.

	// Récupérer le prénom de l'utilisateur
	$usr=$_SESSION['usr'];
	$mdp=$_SESSION['mdp'];
	$sql = "SELECT prenom FROM utilisateurs WHERE mel = '$usr' and mdp = '$mdp'";
	$result = mysqli_query($db,$sql);
	$arr = mysqli_fetch_array($result,MYSQLI_ASSOC);
	$prenom = $arr['prenom'];
	echo("<h2 class='text-center'>Bonjour, " . $prenom . "</h2><br>");
	
	
	// Récupérer les personnes
	$sql = "SELECT id, prenom, nom FROM personnes_cine"; 
	$result = mysqli_query($db,$sql);
	$series = array(); // Initialiser un tableau vide (qui sera un tableau de tableaux)
	if(mysqli_num_rows($result) > 0){ // S'il y a des séries
		while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){ // Tant que le résultat existe
			$tmp = array(
			'id' => $row['id'],
			'prenom' => $row['prenom'],
			'nom' => $row['nom']);
			$series[] = $tmp; // Append le résultat
		}
	}
	
	
	// Rechercher chaque série avec la personne choisie
	if(isset($_POST['submit'])) { // Si on a appuyé sur le bouton
		// Récupérer les valeurs
		if(isset($_POST['nom'])) $id=$_POST['nom'];
		else $id="";
		
		if (isset($id)) {
			if ($id == '*'){
				$sql = "SELECT
						series.id AS id,
						series.date_sortie AS date_sortie,
						series.nom AS nom,
						series.plot AS plot
						FROM series";
			}
			else {
			$sql = "SELECT
						series.id AS id,
						series.date_sortie AS date_sortie,
						series.nom AS nom,
						series.plot AS plot
						FROM series
						INNER JOIN joue_dans ON joue_dans.ref_serie = series.id
						INNER JOIN personnes_cine ON joue_dans.ref_personne_cine = personnes_cine.id
						WHERE
							personnes_cine.id = " . $id;
			}
		}
	}
	else {
		// Rechercher chaque série
		$sql = "SELECT
				series.id AS id,
				series.date_sortie AS date_sortie,
				series.nom AS nom,
				series.plot AS plot
				FROM series";
	}
	
	
	// Récupérer les séries
	$result = mysqli_query($db,$sql);
	$tab_series = array(); // Initialiser un tableau vide (qui sera un tableau de tableaux)
	if(mysqli_num_rows($result) > 0){ // S'il y a des séries
		
		while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){ // Tant que le résultat existe
		
			// Réinstancier le tableau des personnes pour le vider
			$pers_row_res['real'] = 'Inconnu';
			$pers_row_res['acteur'] = 'Inconnu';
		
			// Pour simplifier et éviter une jonction illisible, les calculs sont effectués par épisode
					
			// Nombre d'épisodes
			$epi_sql = 'SELECT count(*) AS nbr from episode WHERE episode.ref_serie = ' . $row['id'];
			$epi_result = mysqli_query($db,$epi_sql);
			if ($epi_result != '') {
				$epi_row = mysqli_fetch_array($epi_result, MYSQLI_ASSOC);
			}
			else $epi_row['nbr'] = 0;
		
			// Réalisateur
			$pers_sql = 'SELECT personnes_cine.prenom, personnes_cine.nom, joue_dans.role from personnes_cine
							INNER JOIN joue_dans ON joue_dans.ref_personne_cine = personnes_cine.id
							INNER JOIN series ON joue_dans.ref_serie = series.id
							WHERE series.id = '. $row['id'];
			$pers_result = mysqli_query($db,$pers_sql);
			if ($pers_result != '') {
				while($pers_row = mysqli_fetch_array($pers_result,MYSQLI_ASSOC)){
					if($pers_row['role'] == 'Réalisateur'){
						$pers_row_res['real'] = $pers_row['prenom'] . ' ' . $pers_row['nom'];
					}
					else if($pers_row['role'] == 'Acteur Principal'){
						$pers_row_res['acteur'] = $pers_row['prenom'] . ' ' . $pers_row['nom'];
					}
				}
			}
		
			$tmp = array(
				'id' => $row['id'],
				'plot' => $row['plot'],
				'nom' => $row['nom'],
				'real' => $pers_row_res['real'],
				'acteur' => $pers_row_res['acteur'],
				'date_sortie' => $row['date_sortie'],
				'nbr_epi' => $epi_row['nbr']);
			$tab_series[] = $tmp; // Append le résultat
		}
	}
	
	
	
	if(isset($result)){
		echo("
			<div class='container'>");
				include("includes/last_episode.php");
		echo("
				<div class='row justify-content-center'>
					<form class='form-inline' name='qui' method=post>
						<label class='mr-sm-2' for='nom'>Choisir un acteur ou réalisateur : </label>
						<select class='mr-sm-2 custom-select' id='nom' name='nom'>");
							echo('<option value=*>Tous</option>');
							for($i=0 ; $i<count($series) ; $i++){	// On propose chaque personne en option.
								echo('<option value=' . $series[$i]['id'] . '>' . $series[$i]['prenom'] . ' ' . $series[$i]['nom'] . '</option>');
							}
		echo("			</select>
						<button type='submit' name='submit' class='btn btn-primary'>Rechercher</button>
					</form>
				</div>
			</div>
		
			<div class='container'>												<!-- Encapsuler la table dans un container pour un rendu propre et uniforme -->
				<div class='table-responsive'>									<!-- Permet d'afficher une scrollbar horizontale si besoin -->
					<table class='table table-dark table-hover table-striped'> 	<!-- Table avec alternance de couleurs et hover au passage de la souris -->
					<caption>Cliquez sur une série pour la visionner.</caption>
						<thead>													<!-- Première ligne avec intitulés -->
							<tr>
								<th scope='col'>Nom</th>
								<th scope='col'>Résumé</th>
								<th scope='col'>Episodes</th>
								<th scope='col'>Réalisateur</th>
								<th scope='col'>Acteur</th>
								<th scope='col'>Sortie</th>
							</tr>
						</thead>
						<tbody>");
						
					for($i=0 ; $i<count($tab_series) ; $i++){	// On propose chaque personne en option.
						// La ligne renvoie vers la page de la série
						echo("
							<tr class='table-row' data-href='/nantflix/serie.php?{$tab_series[$i]['id']}'>
								<td>{$tab_series[$i]['nom']}</td> 				<!-- Les {} sont nécessaire pour éviter l'erreur d'unexpected T_ENCAPSED_AND_WHITESPACE -->
								<td>{$tab_series[$i]['plot']}</td>
								<td>{$tab_series[$i]['nbr_epi']}</td>
								<td>{$tab_series[$i]['real']}</td>
								<td>{$tab_series[$i]['acteur']}</td>
								<td><script>affiche_date('".$tab_series[$i]['date_sortie']."')</script></td>
							</tr>");
					}
		echo("
						</tbody>
					</table>
				</div>
			</div>");
	}
	else {
		echo("Il y a une une erreur.");
	}
?>

    </body>
</html>
