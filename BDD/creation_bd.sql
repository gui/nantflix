﻿-- ---
-- Globals
-- ---

-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
-- SET FOREIGN_KEY_CHECKS=0;


CREATE DATABASE IF NOT EXISTS nantflix;
USE nantflix;


-- ---
-- Table 'utilisateurs'
-- Table contenant les informations sur les utilisateurs de la plateforme.
-- ---

DROP TABLE IF EXISTS `utilisateurs`;
		
CREATE TABLE `utilisateurs` (
  `mel` VARCHAR(150) NOT NULL,
  `mdp` VARCHAR(150) NOT NULL,
  `prenom` VARCHAR(150) NOT NULL,
  `nom` VARCHAR(150) NOT NULL,
  `date_naiss` DATE NOT NULL,
  `tel` INTEGER(10) NOT NULL,
  PRIMARY KEY (`mel`)
) COMMENT 'Table contenant les informations sur les utilisateurs.';

-- ---
-- Table 'series'
-- Les séries enregistrées sur le site.
-- ---

DROP TABLE IF EXISTS `series`;
		
CREATE TABLE `series` (
  `id` INTEGER AUTO_INCREMENT,
  `nom` VARCHAR(150) NOT NULL,
  `date_sortie` DATE DEFAULT NULL,
  `plot` MEDIUMTEXT  DEFAULT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'Les séries enregistrées sur le site.';

-- ---
-- Table 'episode'
-- Les épisodes des séries.
-- ---

DROP TABLE IF EXISTS `episode`;
		
CREATE TABLE `episode` (
  `id` INTEGER AUTO_INCREMENT,
  `ref_serie` INTEGER NOT NULL,
  `numero` INTEGER DEFAULT NULL,
  `nom` VARCHAR(150),
  PRIMARY KEY (`id`)
) COMMENT 'Les épisodes des séries.';

-- ---
-- Table 'historique'
-- Historique de visionnage des utilisateurs.
-- ---

DROP TABLE IF EXISTS `historique`;
		
CREATE TABLE `historique` (
  `id` INTEGER AUTO_INCREMENT,
  `ref_utilisateur` VARCHAR(150) NOT NULL,
  `ref_episode` INTEGER NOT NULL,
  `date_visionnage` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) COMMENT 'Historique de visionnage des utilisateurs.';

-- ---
-- Table 'personnes_cine'
-- Les personnes qui travaillent sur les séries.
-- ---

DROP TABLE IF EXISTS `personnes_cine`;
		
CREATE TABLE `personnes_cine` (
  `id` INTEGER AUTO_INCREMENT,
  `prenom` VARCHAR(150) NOT NULL,
  `nom` VARCHAR(150) NOT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'Les personnes qui travaillent sur les séries.';

-- ---
-- Table 'joue_dans'
-- Table N/N des acteurs ou réalisateurs qui ont travaillé sur telle série.
-- ---

DROP TABLE IF EXISTS `joue_dans`;
		
CREATE TABLE `joue_dans` (
  `ref_personne_cine` INTEGER NOT NULL,
  `ref_serie` INTEGER NOT NULL,
  `role` VARCHAR(50) NULL DEFAULT NULL,
  PRIMARY KEY (`ref_personne_cine`, `ref_serie`)
) COMMENT 'Table N/N des acteurs ou réalisateurs qui ont travaillé sur ';

-- ---
-- Foreign Keys 
-- ---

ALTER TABLE `episode` ADD FOREIGN KEY (ref_serie) REFERENCES `series` (`id`);
ALTER TABLE `historique` ADD FOREIGN KEY (ref_utilisateur) REFERENCES `utilisateurs` (`mel`);
ALTER TABLE `historique` ADD FOREIGN KEY (ref_episode) REFERENCES `episode` (`id`);
ALTER TABLE `joue_dans` ADD FOREIGN KEY (ref_personne_cine) REFERENCES `personnes_cine` (`id`);
ALTER TABLE `joue_dans` ADD FOREIGN KEY (ref_serie) REFERENCES `series` (`id`);

-- ---
-- Table Properties
-- ---

-- ALTER TABLE `utilisateurs` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `series` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `episode` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `historique` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `personnes_cine` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `joue_dans` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ---
-- Test Data
-- ---

-- INSERT INTO `utilisateurs` (`mel`,`mdp`,`prenom`,`nom`,`date_naiss`,`tel`) VALUES
-- ('','','','','','');
-- INSERT INTO `series` (`id`,`nom`,`date_sortie`,`plot`,`Éditer un champ`) VALUES
-- ('','','','','');
-- INSERT INTO `episode` (`id`,`ref_serie`,`numero`,`nom`) VALUES
-- ('','','','');
-- INSERT INTO `historique` (`id`,`ref_utilisateur`,`ref_episode`,`date_visionnage`) VALUES
-- ('','','','');
-- INSERT INTO `personnes_cine` (`id`,`prenom`,`nom`,`Éditer un champ`) VALUES
-- ('','','','');
-- INSERT INTO `joue_dans` (`ref_personne_cine`,`ref_serie`,`role`) VALUES
-- ('','','');