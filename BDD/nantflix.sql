﻿-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : lun. 23 mars 2020 à 18:19
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP : 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `nantflix`
--

-- --------------------------------------------------------

--
-- Structure de la table `episode`
--

CREATE TABLE `episode` (
  `id` int(11) NOT NULL,
  `ref_serie` int(11) NOT NULL,
  `numero` int(11) DEFAULT NULL,
  `nom` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Les épisodes des séries.';

-- --------------------------------------------------------

--
-- Structure de la table `historique`
--

CREATE TABLE `historique` (
  `id` int(11) NOT NULL,
  `ref_utilisateur` varchar(150) NOT NULL,
  `ref_episode` int(11) NOT NULL,
  `date_visionnage` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Historique de visionnage des utilisateurs.';

-- --------------------------------------------------------

--
-- Structure de la table `joue_dans`
--

CREATE TABLE `joue_dans` (
  `ref_personne_cine` int(11) NOT NULL,
  `ref_serie` int(11) NOT NULL,
  `role` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Table N/N des acteurs ou réalisateurs qui ont travaillé sur ';

-- --------------------------------------------------------

--
-- Structure de la table `personnes_cine`
--

CREATE TABLE `personnes_cine` (
  `id` int(11) NOT NULL,
  `prenom` varchar(150) NOT NULL,
  `nom` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Les personnes qui travaillent sur les séries.';

-- --------------------------------------------------------

--
-- Structure de la table `series`
--

CREATE TABLE `series` (
  `id` int(11) NOT NULL,
  `nom` varchar(150) NOT NULL,
  `date_sortie` date DEFAULT NULL,
  `plot` mediumtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Les séries enregistrées sur le site.';

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

CREATE TABLE `utilisateurs` (
  `mel` varchar(150) NOT NULL,
  `mdp` varchar(150) NOT NULL,
  `prenom` varchar(150) NOT NULL,
  `nom` varchar(150) NOT NULL,
  `date_naiss` date NOT NULL,
  `tel` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Table contenant les informations sur les utilisateurs.';

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `episode`
--
ALTER TABLE `episode`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ref_serie` (`ref_serie`);

--
-- Index pour la table `historique`
--
ALTER TABLE `historique`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ref_utilisateur` (`ref_utilisateur`),
  ADD KEY `ref_episode` (`ref_episode`);

--
-- Index pour la table `joue_dans`
--
ALTER TABLE `joue_dans`
  ADD PRIMARY KEY (`ref_personne_cine`,`ref_serie`),
  ADD KEY `ref_serie` (`ref_serie`);

--
-- Index pour la table `personnes_cine`
--
ALTER TABLE `personnes_cine`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `series`
--
ALTER TABLE `series`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD PRIMARY KEY (`mel`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `episode`
--
ALTER TABLE `episode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `historique`
--
ALTER TABLE `historique`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `personnes_cine`
--
ALTER TABLE `personnes_cine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `series`
--
ALTER TABLE `series`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `episode`
--
ALTER TABLE `episode`
  ADD CONSTRAINT `episode_ibfk_1` FOREIGN KEY (`ref_serie`) REFERENCES `series` (`id`);

--
-- Contraintes pour la table `historique`
--
ALTER TABLE `historique`
  ADD CONSTRAINT `historique_ibfk_1` FOREIGN KEY (`ref_utilisateur`) REFERENCES `utilisateurs` (`mel`),
  ADD CONSTRAINT `historique_ibfk_2` FOREIGN KEY (`ref_episode`) REFERENCES `episode` (`id`);

--
-- Contraintes pour la table `joue_dans`
--
ALTER TABLE `joue_dans`
  ADD CONSTRAINT `joue_dans_ibfk_1` FOREIGN KEY (`ref_personne_cine`) REFERENCES `personnes_cine` (`id`),
  ADD CONSTRAINT `joue_dans_ibfk_2` FOREIGN KEY (`ref_serie`) REFERENCES `series` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
