<?php
/* Page de lecture d'une série */
	include("includes/base.php");					// Base : header et navbar
	include("includes/session.php"); 				// Vérifie que l'utilisateur est connecté.
	
	function no_episode() {
		echo("
			<div class='container'>
				<h2 class='text-center'>Cette série n'a aucun épisode.</h2><br>
				<div class='row justify-content-center'>
					<a href='/nantflix/series.php' class='btn btn-primary btn-lg active' role='button' aria-pressed='true'>Voir la liste de séries</a>
				</div>
			</div>
		");
	}
	
	$arg = explode("&", $_SERVER["QUERY_STRING"]); 	// Récupérer les arguments de la requête
	if (!empty($arg) and isset($arg[0])) {			// Un ID de série est spécifié
		$sql = "SELECT * FROM series WHERE id = " . $arg[0]; 
		$result = mysqli_query($db, $sql);			// On récupère la série avec cet ID
		
		if(mysqli_num_rows($result) > 0){ 			// S'il y a des séries
			$series_row = mysqli_fetch_array($result, MYSQLI_ASSOC);
			$series_nom = $series_row['nom'];
			if (isset($arg[1])){					// Si un ID d'épisode est spécifié
				$sql2 = "SELECT * FROM episode WHERE id={$arg[1]}"; 
				$result2 = mysqli_query($db, $sql2);	// On récupère la série avec cet ID

				if(mysqli_num_rows($result2) > 0){ 	// S'il y a des épisodes
				// ICI VERIFIER SI DEJA VU OU NON (peut-être)
					header('Location: ' . "/nantflix/includes/redirect.php?dir=/nantflix/play.php?{$arg[0]}-{$arg[1]}", true); 	// On rediriger vers la page de séries
				}
				
				else {								// Aucun épisode avec cet ID
					$sql3 = "SELECT * FROM episode WHERE ref_serie={$arg[0]} ORDER BY numero"; 
				}
			}
			
			else {									// La requête ne contient pas d'ID d'épisode
				$sql3 = "SELECT * FROM episode WHERE ref_serie={$arg[0]} ORDER BY numero"; 
			}
		}
		
		else {										// Numéro de série erroné
			header('Location: ' . "series.php", true); // On rediriger vers la page de séries
		}
	}
	
	else {											// Aucun ID de série spécifié (requête invalide)
		header('Location: ' . "series.php", true); 	// On rediriger vers la page de séries
	}
	
	if(isset($sql3)) {								// S'il est nécessaire de récupérer les épisodes
		$result3 = mysqli_query($db, $sql3);
		$date_vision = array();						// Dates de visionnage par épisode
		while($row = mysqli_fetch_array($result3, MYSQLI_ASSOC)) {	// Chaque épisode retrivé
			$sql_date = "SELECT date_visionnage FROM historique WHERE (ref_utilisateur = '{$_SESSION['usr']}' and ref_episode = {$row['id']})";
			$result_date = mysqli_query($db, $sql_date);
			if(mysqli_num_rows($result_date) > 0){
				$date = mysqli_fetch_array($result_date, MYSQLI_ASSOC);
				$date_visio[] = $date['date_visionnage']; // Ce serait bien d'adapter la fonction JS pour ça.
			}
			else $date_visio[] = "jamais";
		}
		$result3 = mysqli_query($db, $sql3);		// Flusher pour mysqli_fetch_array
		
		// Le premier épisode non vu
		$sql_min = "SELECT min(numero) AS num FROM episode
						WHERE (
							episode.id NOT IN (
								SELECT ref_episode FROM historique
									WHERE ref_utilisateur = '{$_SESSION['usr']}') and
							episode.ref_serie = {$arg[0]}
						)";
		$result_min = mysqli_query($db, $sql_min);
		$row_min = mysqli_fetch_array($result_min, MYSQLI_ASSOC);
		$min = $row_min['num'];
		
		// Le premier épisode disponible (pas forcément numéro 1)
		$sql_prem = "SELECT min(numero) AS num FROM episode
						WHERE (episode.ref_serie = {$arg[0]})";
		$result_prem = mysqli_query($db, $sql_prem);
		$row_prem = mysqli_fetch_array($result_prem, MYSQLI_ASSOC);
		$prem = $row_prem['num'];
	}
	
?>

<!--

// si l'utilisateur à déjà commencé, lui proposer de reprendre directement avec un bouton référencant vers /lire.php?n/i
// avec n l'id de la série et i l'id de l'épisode

// afficher ensuite chaque épisode avec des liens de lecture-->

			<div class='container'>
			
<?php
	if(isset($result3)) {
		if (mysqli_num_rows($result3) > 0){
			if($min) {								// Si au moins un épisode n'a pas été vu :
				if ($min != $prem) {				// Si l'utilisateur a déjà commencé la série :
					echo("<h2 class='text-center'>{$series_nom}, reprenez où vous en étiez :</h2><br>
					<div class='row justify-content-center'>
						<a href='/nantflix/includes/redirect.php?dir=/nantflix/play.php?{$arg[0]}-{$min}' class='btn btn-primary btn-lg active' role='button' aria-pressed='true'>Episode {$min}</a>
					</div><br>");
				}
				else {								// Série non commencée
					echo("<h2 class='text-center'>{$series_nom}</h2><br>");
				}
			}
			else {									// Il ne reste aucun épisode à voir.
				echo("<h2 class='text-center'>Bravo, vous avez terminé la {$series_nom} !</h2><br>");
			}
			
				echo("
					<table class='table table-dark table-hover table-striped'>
					<caption>Cliquez sur un épisode pour le visionner.</caption>
						<thead>
							<tr>
								<th scope='col'>N°</th>
								<th scope='col'>Nom</th>
								<th scope='col'>Vu</th>
							</tr>
						</thead>
						<tbody>");
								$n=0;				// Pour naviguer dans le tableau de date de visionnage
								while($row = mysqli_fetch_array($result3, MYSQLI_ASSOC)) {	// Chaque épisode retrivé
									// La ligne renvoie vers la page de l'épisode
									echo("
										<tr class='table-row' data-href='/nantflix/includes/redirect.php?dir=/nantflix/play.php?{$arg[0]}-{$row['numero']}'>
											<td>{$row['numero']}</td>
											<td>{$row['nom']}</td>
											<td><script>affiche_date_epi('{$date_visio[$n]}')</script></td>
										</tr>");
									$n++;
								}
				echo("
						</tbody>
					</table>
				</div>");
		}
		else {
			no_episode();
		}
	}
	else {
		no_episode();
	}
?>
    </body>
</html>
