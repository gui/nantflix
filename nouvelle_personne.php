<!DOCTYPE html>
<!-- Page privée de d'insertion d'épisode -->
<html lang="fr">

<?php
	include("includes/base.php");

	include("includes/session.php"); // Vérifie que l'utilisateur est connecté.
	
	$sql = "SELECT nom FROM series"; 
	$result = mysqli_query($db,$sql); // Récupérer tous les séries
	$series = array(); // Initialiser un tableau vide
	if(mysqli_num_rows($result) > 0){ // S'il y a des séries
		while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){ // Tant que le résultat existe
			$series[] = $row['nom']; // Append le résultat
		}
	}
	else {
		echo ("Il n'y a aucune série. Ajoutez-en une <a href=nouvelle_serie.php>ici</a>");
	}
	
	if(isset($_POST['submit'])) { // Si on a appuyé sur le bouton
		
		// Récupérer les valeurs
		if(isset($_POST['prenom'])) $prenom=mysqli_real_escape_string($db, $_POST['prenom']);
		else $prenom="";
		if(isset($_POST['nom'])) $nom=mysqli_real_escape_string($db, $_POST['nom']);
		else $nom="";
		if(isset($_POST['role'])) $role=mysqli_real_escape_string($db, $_POST['role']);
		else $role="";
		$i=$_POST['serie'];
		
		//On vérifie si les champs sont valides
		if(empty($prenom) OR empty($nom) OR empty($role)){
			echo("
				<div class='container'>
					<div class='row justify-content-center'>
						<div class='col-sm-4'>
							<div class='alert alert-danger' role='alert'>
								Attention, les prénom, nom et rôle des personnes ne peuvent rester vides !
							</div>
						</div>
					</div>
				</div>");
		}
		else {
			if($_SERVER["REQUEST_METHOD"] == "POST") {
				// La vérifier
				if (!$db) { die("Erreur avec la base de données : " . mysqli_connect_error()); }
				
				$query1 = "INSERT INTO personnes_cine(prenom, nom) VALUES('$prenom', '$nom')";
				$query2 = "SET @personne_id = LAST_INSERT_ID()";
				$query3 = "INSERT INTO joue_dans(ref_serie, ref_personne_cine, role) VALUES (
					(SELECT id FROM series WHERE(nom = '$series[$i]')), @personne_id, '$role')";
				if(mysqli_query($db, $query1) AND mysqli_query($db, $query2) AND mysqli_query($db, $query3)){
					echo("
						<div class='container'>
							<div class='row justify-content-center'>
								<div class='col-sm-4'>
									<div class='alert alert-success' role='alert'>
										$prenom $nom a été ajouté(e).
									</div>
								</div>
							</div>
						</div>");
				}
				else{
					echo("
						<div class='container'>
							<div class='row justify-content-center'>
								<div class='col-sm-6'>
									<div class='alert alert-danger' role='alert'>
										Une erreur est intervenue lors de l introduction dans la base." . $query1 . "<br>" . $query2 . "<br>" . $query3 . "
									</div>
								</div>
							</div>
						</div>");
				}

				mysqli_close($db);
			}
		}
	} // fin isset
	

?>
		<h2 class='text-center'>Ajouter votre série préférée !</h2><br>
		
		
		<div class='container'> 						<!-- Container est l'élément principal nécessaire -->
			<div class='row justify-content-center'> 	<!-- Grille : une ligne -->
				<div class='col-6'> 					<!-- Colonne de taille 6 -->
					<form
						name="nouvelle_personne"
						method=post
						style="text-align: center;"
						enctype="multipart/form-data">
						<div class='form-group row'> 	<!-- Formulaire en lignes, .col-*-* pour la taille des labels et controls -->
														<!-- Label pour nom d'une taille de 4 -->
							<label for='serie' class='col-sm-4 col-form-labem'>Ajouter à la série</label>
							<div class='col-sm-8'>
								<select
									name="serie"
									id='serie'
									class='custom-select'>
										<?php
											for($i=0 ; $i<count($series) ; $i++){	// Pour chaque série on met une option.
											//Je voulais foreach mais foreach a un problème avec les espaces
												echo("<option value=" . $i . ">" . $series[$i] . "</option>");
											}
										?>
									</select>
							</div>
						</div>
						
						<div class='form-row'>	<!-- On opte pour un placement des deux champs en ligne -->
							<div class='form-group col-md-6'>
								<label for='prenom' class='col-sm-4 col-form-labem'>Prénom</label>
								<input
									type=text
									class='form-control'
									id='prenom'
									name="prenom"
									placeholder='Prénom'
									autocomplete="off"
									pattern="[A-Z].*" 
									title="Le prénom doit commencer par une majuscule">
							</div>
							<div class='form-group col-md-6'>
								<label for='nom' class='col-sm-4 col-form-labem'>Nom</label>
								<input
									type=text
									class='form-control'
									id='nom'
									name="nom"
									placeholder='Nom'
									autocomplete="off"
									pattern="[A-Z].*" 
									title="Le nom doit commencer par une majuscule">
							</div>
						</div>
						
						<div class='form-group row'>
							<label for='role' class='col-sm-4 col-form-labem'>Rôle</label>
							<div class='col-sm-8'>
							<!-- On force le choix entre Acteur, Réalisateur ou Producteur. 
								Pour bien faire il faudrait limiter le choix à un seul réalisateur et acteur principal, 
								mais la réalité est souvent plus complexe. -->
								<select class='mr-sm-2 custom-select' id='role' name='role'>
									<option value='Réalisateur'>Acteur</option>
									<option value='Acteur Principal	'>Acteur Principal</option>
									<option value='Réalisateur'>Réalisateur</option>
									<option value='Réalisateur'>Producteur</option>
								</select>
							</div>
						</div>

						<div class='form-group row'>
							<div class='col-sm-11'>
								<button class='btn btn-primary' name='submit' type='submit'>Ajouter</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		
    </body>
</html>
