<?php
/* Fermer la session d'un utilisateur et le rediriger vers l'accueil */
	include("includes/config.php");
	session_start();
	include("includes/session.php"); // On vérifie que l'utilisateur est bien connecté
	// On fermer la session :
	// supprimer les variables de la session :
	session_unset();
	// detruire la session
	session_destroy();
	
	header("Location: index.php", true,); // rediriger vers l'accueil des utilisateurs non connectés
?>