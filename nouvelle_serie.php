<!DOCTYPE html>
<!-- Page privée de d'insertion de séries -->
<html lang="fr">

<?php
	// S'il y a des éléments de navbar supplémentaires spécifiques à cette page à ajouter
	$navSup = "";
	include("includes/base.php");

	include("includes/session.php"); // Vérifie que l'utilisateur est connecté.

	$usr=$_SESSION['usr'];
	$mdp=$_SESSION['mdp'];
	$sql = "SELECT prenom FROM utilisateurs WHERE mel = '$usr' and mdp = '$mdp'";
	$result = mysqli_query($db,$sql);
	$arr = mysqli_fetch_array($result,MYSQLI_ASSOC);
	$prenom = $arr['prenom'];
	
	if(isset($_POST['submit'])) { // Si on a appuyé sur le bouton
		
		// Récupérer les valeurs
		if(isset($_POST['nom'])) $nom=mysqli_real_escape_string($db, $_POST['nom']);
		else $nom="";
		if(isset($_POST['date_sortie'])) $date_sortie=$_POST['date_sortie'];
		else $date_sortie="";
		if(isset($_POST['plot'])) $plot=mysqli_real_escape_string($db, $_POST['plot']);
		else $plot="";
		
		//On vérifie si les champs sont valides
		if(empty($nom) OR empty($date_sortie) OR empty($plot)){
			echo("
				<div class='container'>
					<div class='row justify-content-center'>
						<div class='col-sm-4'>
							<div class='alert alert-success' role='alert'>
								Attention, les champs Nom, Date et Résumé ne peuvent pas rester vides !
							</div>
						</div>
					</div>
				</div>");
		}
		else {
			if($_SERVER["REQUEST_METHOD"] == "POST") {
				// La vérifier
				if (!$db) { die("Erreur avec la base de données : " . mysqli_connect_error()); }
				
				$query = "INSERT INTO series(nom, date_sortie, plot)
					VALUES('$nom', '$date_sortie', '$plot')";
				if(mysqli_query($db, $query)){
					echo("
						<div class='container'>
							<div class='row justify-content-center'>
								<div class='col-sm-4'>
									<div class='alert alert-success' role='alert'>
										La série a été ajoutée.
									</div>
								</div>
							</div>
						</div>");
				}
				else{
					echo("
						<div class='container'>
							<div class='row justify-content-center'>
								<div class='col-sm-4'>
									<div class='alert alert-danger' role='alert'>
										Une erreur est intervenue lors de l introduction dans la base.
									</div>
								</div>
							</div>
						</div>");
				}
				mysqli_close($db);
			}
		}
	} // fin isset

?>
		<h2 class='text-center'>Bienvenue, <?php echo($prenom) ?>. Ajoutez votre série préférée !</h2><br>
		
		
		
		<div class='container'> 						<!-- Container est l'élément principal nécessaire -->
			<div class='row justify-content-center'> 	<!-- Grille : une ligne -->
				<div class='col-6'> 					<!-- Colonne de taille 6 -->
					<form
						name=""
						method=post
						style="text-align: center;"
						enctype="multipart/form-data">
						<div class='form-group row'> 	<!-- Formulaire en lignes, .col-*-* pour la taille des labels et controls -->
														<!-- Label pour nom d'une taille de 4 -->
							<label for='nom' class='col-sm-4 col-form-labem'>Nom de la série</label>
							<div class='col-sm-7'>
								<input
									type='text'
									class='form-control'
									id='nom'
									name='nom'
									placeholder='Nom de la série'
									autofocus>
							</div>
						</div>
						
						<div class='form-group row'>
							<label for='plot' class='col-sm-4 col-form-labem'>Résumé</label>
							<div class='col-sm-7'>
								<input
									type='text'
									class='form-control'
									id='plot'
									name='plot'
									placeholder='Résumé'
									autofocus='off'>
							</div>
						</div>
						
						<div class='form-group row'>
							<label for='date_sortie' class='col-sm-4 col-form-labem'>Date de sortie</label>
							<div class='col-sm-7'>
								<input
									type='date'
									class='form-control'
									id='date_sortie'
									name='date_sortie'
									placeholder='Date de sortie'
									autofocus='off'>
							</div>
						</div>
						
						<div class='form-group row'>
							<div class='col-sm-11'>
								<button class='btn btn-primary' name='submit' type='submit'>Ajouter</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		
    </body>
</html>
