<?php
/* Page de lecture d'un épisode */
	include("includes/base.php");					// Base : header et navbar
	include("includes/session.php"); 				// Vérifie que l'utilisateur est connecté.
	
	$arg = explode("-", $_SERVER["QUERY_STRING"]); 	// Récupérer les arguments de la requête
	if (!isset($_POST['vu'])) if (!$_SESSION['play']) header('Location: serie.php?' . $arg[0], true); // Si l'on n'a pas été redirigé depuis une page (requête contrôlée), on renvoie
	$_SESSION['play'] = false;						// On reset
	
	// Lien pour Tous les épisodes
	$lien_serie = "/nantflix/serie.php?" . $arg[0];
	
	
	
	// Lien pour épisode suivant
	$sql_tous = "SELECT * FROM episode WHERE ref_serie={$arg[0]} ORDER BY numero";
	$result_tous = mysqli_query($db, $sql_tous);
	if (mysqli_num_rows($result_tous) > 0){
		while($row = mysqli_fetch_array($result_tous, MYSQLI_ASSOC)) {
			if ($row['numero'] > $arg[1]) {
				$prochain_num = $row['numero'];
				break;
			}
		}
	}
	if (isset($prochain_num)) {
		$bouton_suivant = "<a href='/nantflix/includes/redirect.php?dir=/nantflix/play.php?{$arg[0]}-{$prochain_num}' class='btn btn-primary btn-sm active' role='button' aria-pressed='true'>Episode suivant</a>";
	}
	else {
		$bouton_suivant = "<a href=# class='btn btn-primary btn-sm disabled' role='button' aria-disabled='true'>Episode suivant</a>";
	}
	
	// Nom de la série
	$sql_nom = "SELECT nom FROM series WHERE id = {$arg[0]}";
	$result_nom = mysqli_query($db, $sql_nom);
	$row_nom = mysqli_fetch_array($result_nom, MYSQLI_ASSOC);
	
	// Nom de l'épisode
	$sql_epi = "SELECT * FROM episode WHERE (ref_serie = {$arg[0]} and numero = {$arg[1]})";
	$result_epi = mysqli_query($db, $sql_epi);
	$row_epi = mysqli_fetch_array($result_epi, MYSQLI_ASSOC);
	$titre = "<center><h2>Vous regardez {$row_nom['nom']} épisode {$arg[1]} : {$row_epi['nom']}</h2></center>";
	
	if(isset($_POST['vu'])) {
		$sql = "INSERT INTO historique(ref_episode, ref_utilisateur) VALUES({$row_epi['id']}, '{$_SESSION['usr']}')";
		
		if(mysqli_query($db, $sql)){				// Insertion réussie
			echo("
				<div class='container'>
					<div class='row justify-content-center'>
						<div class='col-sm-6'>
							<div class='alert alert-success' role='alert'>
								Episode marqué comme vu.
							</div>
						</div>
					</div>
				</div>");
		}
		else{										// Erreur
			echo('<center><h2> Une erreur est intervenue lors de l introduction dans la base.</h2></center>' . $sql);
		}
	}		

?>
	<div class='container'>
		<?php echo($titre) ?>
		
		<br>
		<img src='images/video.png' class='img-fluid mx-auto d-block' alt=<?php echo($row_epi['nom']) ?>>
		<br>
		
		<div class='row'>
			<div class='col-sm text-left'>
				<a href='<?php echo($lien_serie); ?>' class='btn btn-primary btn-sm active' role='button' aria-pressed="true">Tous les épisodes</a>
			</div>
			<div class="col-sm text-center">
				<form method='post'>
					<button type='submit' name='vu' class="btn btn-secondary btn-sm active" role="button" aria-pressed="true">Marquer comme vu</button>
				</form>
			</div>
			<div class="col-sm text-right">
				<?php echo($bouton_suivant); ?>
			</div>
		</div>
	</div>
	

    </body>
</html>
