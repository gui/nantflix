<?php
/* Page d'inscription d'un nouvel utilisateur */
	include("includes/base.php");

	if (isset($_SESSION['usr'])) { header('Location: ' . "series.php", true); } // Si l'utilisateur est déjà connecté, on le redirige
	
	if(isset($_POST['submit'])) { // Si on a appuyé sur le bouton
		
		// Récupérer les valeurs
		if(isset($_POST['mel'])) $mel=mysqli_real_escape_string($db, $_POST['mel']);
		else $mel="";
		if(isset($_POST['prenom'])) $prenom=mysqli_real_escape_string($db, $_POST['prenom']);
		else $prenom="";
		if(isset($_POST['nom'])) $nom=mysqli_real_escape_string($db, $_POST['nom']);
		else $nom="";
		if(isset($_POST['daten'])) $date_naiss=$_POST['daten'];
		else $date_naiss="";
		if(isset($_POST['tel'])) $tel=$_POST['tel'];
		else $tel="";
		// Il faudrait hasher le mot de passe !
		if(isset($_POST['password'])) $password=$_POST['password'];
		else $password="";
		
		//On vérifie si les champs sont valides
		If(empty($password) OR empty($nom) OR empty($prenom) OR empty($mel)) {
			echo("
				<div class='container'>
					<div class='row justify-content-center'>
						<div class='col-sm-6'>
							<div class='alert alert-danger' role='alert'>
								Attention, les champs Nom, Prenom, Mot de passe et Adresse de messagerie ne peuvent pas rester vides !
							</div>
						</div>
					</div>
				</div>");
		}
		else {
			// Créer la connexion
			if($_SERVER["REQUEST_METHOD"] == "POST") {
				// La vérifier
				if (!$db) { die("Erreur avec la base de données : " . mysqli_connect_error()); }
				
				$query = "INSERT INTO utilisateurs(mel, prenom, nom, date_naiss, tel, mdp)
					VALUES('$mel', '$prenom', '$nom', '$date_naiss', '$tel', '$password')";
				if(mysqli_query($db, $query)){
					// Afficher que c'est bon ce qui ne sert pas à grand chose vu qu'on redirige.
					// echo('<center><h2> Vos informations on été ajoutées.</h2></center>');
					
					// Actualiser la session
					$_SESSION['usr'] = $mel;
					$_SESSION['mdp'] = $password;
					header("Location: series.php", true,); // rediriger vers la page de séries
				}
				else{
					echo('<center><h2> Une erreur est intervenue lors de l introduction dans la base.</h2></center>' . $query);
				}
				mysqli_close($db);
			}
		}
	} // fin isset
	

?>

		<div class='container'> 						<!-- Container est l'élément principal nécessaire -->
			<div class='row justify-content-center'> 	<!-- Grille : une ligne -->
				<div class='col-6'> 					<!-- Colonne de taille 6 -->
					<form
						name="inscription"
						method=post
						style="text-align: center;"
						enctype="multipart/form-data">
						
						<div class='form-row'>			<!-- On opte pour un placement des deux champs en ligne -->
							<div class='form-group col-md-6'>	<!-- Chacun des row fait 6 de large -->
								<label for='prenom' class='col-sm-4 col-form-labem'>Prénom</label>
								<input
									type=text
									class='form-control'
									id='prenom'
									name="prenom"
									placeholder='Prénom'
									autocomplete="on"
									pattern="[A-Z].*" 
									title="Le prénom doit commencer par une majuscule">
														<!-- L'autocomplete permet d'autoriser le navigateur à remplir automatiquement -->
							</div>
							<div class='form-group col-md-6'>
								<label for='nom' class='col-sm-4 col-form-labem'>Nom</label>
								<input
									type=text
									class='form-control'
									id='nom'
									name="nom"
									placeholder='Nom'
									autocomplete="on"
									pattern="[A-Z].*" 
									title="Le nom doit commencer par une majuscule">
							</div>
						</div>
						
						<div class='form-group row'> 	<!-- Formulaire en lignes, .col-*-* pour la taille des labels et controls -->
														<!-- Label pour identifiant d'une taille de 4 -->
							<label for='identifiant' class='col-sm-4 col-form-labem'>Adresse de courriel (votre identifiant)</label>
							<div class='col-sm-8'>
								<input
									type='email'
									class='form-control'
									id='identifiant'
									name='mel'
									placeholder='Adresse de courriel'
									autofocus
									autocomplete="on">
							</div>
						</div>
						
						<div class='form-group row'>
							<label for='daten' class='col-sm-4 col-form-labem'>Date de naissance</label>
							<div class="col-sm-8">
								<input
									type='date'
									class='form-control'
									id='daten'
									name='daten'
									placeholder='Date de naissance'>
							</div>
						</div>
						
						<div class='form-group row'>
							<label for='tel' class='col-sm-4 col-form-labem'>Numéro de téléphone</label>
							<div class="col-sm-8">
								<input
									type='tel'
									class='form-control'
									id='tel'
									name='tel'
									pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$"
									placeholder='Numéro de téléphone'>
							</div>	<!-- Le type tel permet d'afficher un petit clavier à chifres sur les téléphones -->
						</div>

						<div class='form-group row'>
							<label for='password' class='col-sm-4 col-form-labem'>Mot de passe</label>
							<div class="col-sm-8">
								<input
									type='password'
									class='form-control'
									id='password'
									name='password'
									pattern="(?=\S*?[A-Z])(?=\S*?[a-z])(?=\S*?[0-9])(?=\S*?[^a-zA-Z0-9]).{4,8}"
									title="Le mot de passe doit contenir au moins une lettre minuscule, une majuscule, un chiffre et un caractère spécial et faire entre 4 et 8 caractères."
									placeholder='Mot de passe'>
							</div>
						</div>
						
						<div class='form-group row'>
							<div class='col-sm-12'> 	<!-- 4+8 = 12, il fait la même taille que la somme des row au-dessus -->
								<button class='btn btn-primary' type='submit' name='submit'>S'enregistrer</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		
    </body>
</html>
