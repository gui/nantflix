<?php
/* Connexion d'un utilisateur déjà enregistré */
	include("includes/base.php");
	if (isset($_SESSION['usr'])) { header('Location: ' . "series.php", true); } // Si l'utilisateur est déjà connecté, on le redirige
	
	
	if($_SERVER["REQUEST_METHOD"] == "POST") {
		$usr = mysqli_real_escape_string($db, $_POST['identifiant']);
		$mdp = mysqli_real_escape_string($db, $_POST['password']);
	  
		if (empty($usr) OR empty($mdp)) {
			echo("
				<div class='container'>
					<div class='row justify-content-center'>
						<div class='col-sm-6'>
							<div class='alert alert-danger' role='alert'>
								Les deux champs sont néessaires.
							</div>
						</div>
					</div>
				</div>");
		}
		else {
			$sql = "SELECT mel FROM utilisateurs WHERE mel = '$usr' and mdp = '$mdp'";
			$result = mysqli_query($db,$sql);
			if ($result){
				$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
				$count = mysqli_num_rows($result);
		
				if($count == 1) { // Déjà enregistré
					// Actualiser la session
					$_SESSION['usr'] = $usr;
					$_SESSION['mdp'] = $mdp;
					
					header('Location: ' . "series.php", true); // Rediriger vers la page de séries
				}
			}
			else {
				echo("
					<div class='container'>
						<div class='row justify-content-center'>
							<div class='col-sm-6'>
								<div class='alert alert-danger' role='alert'>
									Vous n'êtes pas enregistré.
								</div>
							</div>
						</div>
					</div>");
			}
		}
	}

?>
		<div class='container'> 						<!-- Container est l'élément principal nécessaire -->
			<div class='row justify-content-center'> 	<!-- Grille : une ligne -->
				<div class='col-6'> 					<!-- Colonne de taille 6 -->
					<form
						name='connexion'
						method=post
						style='text-align: center;'>
						<div class='form-group row'> 	<!-- Formulaire en lignes, .col-*-* pour la taille des labels et controls -->
														<!-- Label pour identifiant d'une taille de 4 -->
							<label for='identifiant' class='col-sm-4 col-form-labem'>Adresse de courriel</label>
							<div class='col-sm-7'>
								<input
									type='email'
									class='form-control'
									id='identifiant'
									name='identifiant'
									placeholder='Adresse de courriel'
									autofocus>
							</div>
						</div>
						<div class='form-group row'>
							<label for='password' class='col-sm-4 col-form-labem'>Mot de passe</label>
							<div class="col-sm-7">
								<input
									type='password'
									class='form-control'
									id='password'
									name='password'
									placeholder='Mot de passe'>
							</div>
						</div>
						<div class='form-group row'>
							<div class='col-sm-11'> 	<!-- 4+7 = 11, il fait la même taille que la somme des row au-dessus -->
								<button class='btn btn-primary' type='submit'>Se connecter</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		
    </body>
</html>

