<!DOCTYPE html>
<!-- Page privée de d'insertion d'épisode -->

<?php
	include("includes/base.php");
	
	include("includes/session.php"); // Vérifie que l'utilisateur est connecté.
	
	// Je n'y avais pas pensé, mais on aurait pu faire une clef primaire composée de ref_serie et numero (d'épisode).
	$sql = "SELECT nom FROM series"; 
	$result = mysqli_query($db,$sql); // Récupérer tous les séries
	$series = array(); // Initialiser un tableau vide
	if(mysqli_num_rows($result) > 0){ // S'il y a des séries
		while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){ // Tant que le résultat existe
			$series[] = $row['nom']; // Append le résultat
		}
	}
	else {
		echo ("<h2 class='text-center'>Il n'y a aucune série. Ajoutez-en une <a href=nouvelle_serie.php>ici</h2><br>");
	}
	
	if(isset($_POST['submit'])) { // Si on a appuyé sur le bouton
		
		// Récupérer les valeurs
		if(isset($_POST['nom'])) $nom=mysqli_real_escape_string($db, $_POST['nom']);
		else $nom="";
		if(isset($_POST['numero'])) $numero=$_POST['numero'];
		else $numero="";
		$i=$_POST['serie'];
		
		//On vérifie si les champs sont valides
		If(empty($nom) OR empty($numero)){
			echo("
				<div class='container'>
					<div class='row justify-content-center'>
						<div class='col-sm-6'>
							<div class='alert alert-danger' role='alert'>
								Attention, les nom et numéro d'épisode ne peuvent rester vides !
							</div>
						</div>
					</div>
				</div>");
		}
		else {
			if($_SERVER["REQUEST_METHOD"] == "POST") {
				// La vérifier
				if (!$db) { die("Erreur avec la base de données : " . mysqli_connect_error()); }
				
				// On vérifie s'il y a déjà un épisode à ce numéro
				$query = "SELECT episode.id from episode
					INNER JOIN series ON series.id = episode.ref_serie
					where (episode.numero = $numero AND series.nom = '$series[$i]')";
				$result = mysqli_query($db, $query);
				if ($result){
					$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
					$count = mysqli_num_rows($result);
		
					if($count) {
						echo("
							<div class='container'>
								<div class='row justify-content-center'>
									<div class='col-sm-6'>
										<div class='alert alert-danger' role='alert'>
											Il y a déjà un épisode à ce numéro pour cette série.
										</div>
									</div>
								</div>
							</div>");
					}
					else {
						// L'épisode n'existe pas, on insère
						$query = "INSERT INTO episode(nom, numero, ref_serie)
							VALUES('$nom', '$numero', 
								(SELECT id FROM series WHERE(nom = '$series[$i]')))";
						if(mysqli_query($db, $query)){
							echo("
								<div class='container'>
									<div class='row justify-content-center'>
										<div class='col-sm-6'>
											<div class='alert alert-success' role='alert'>
												L'épisode a été ajouté.
											</div>
										</div>
									</div>
								</div>");
						}
						else{
							echo("
								<div class='container'>
									<div class='row justify-content-center'>
										<div class='col-sm-6'>
											<div class='alert alert-danger' role='alert'>
												Une erreur est intervenue lors de l introduction dans la base." . $query . "
											</div>
										</div>
									</div>
								</div>");
						}
					}
				}
				else {
					// L'épisode n'existe pas, on insère
					$query = "INSERT INTO episode(nom, numero, ref_serie)
						VALUES('$nom', '$numero', 
							(SELECT id FROM series WHERE(nom = '$series[$i]')))";
					if(mysqli_query($db, $query)){
						echo("
							<div class='container'>
								<div class='row justify-content-center'>
									<div class='col-sm-6'>
										<div class='alert alert-success' role='alert'>
											L'épisode a été ajouté.
										</div>
									</div>
								</div>
							</div>");
					}
					else{
						echo("
							<div class='container'>
								<div class='row justify-content-center'>
									<div class='col-sm-6'>
										<div class='alert alert-danger' role='alert'>
											Une erreur est intervenue lors de l introduction dans la base." . $query . "
										</div>
									</div>
								</div>
							</div>");
					}
				}
				mysqli_close($db);
			}
		}
	} // fin isset
	

?>
		<h2 class='text-center'>Ajouter un épisode.</h2><br>
		
		
		
		<div class='container'>
			<div class='row justify-content-center'>
				<div class='col-6'>
					<form
						name="nouvelle_serie"
						method=post
						style="text-align: center;"
						enctype="multipart/form-data">
						<div class='form-group row'>
							<label for='serie' class='col-sm-4 col-form-labem'>Ajouter à la série</label>
							<div class='col-sm-8'>
								<select
									name="serie"
									id='serie'
									class='custom-select'>
										<?php
											for($i=0 ; $i<count($series) ; $i++){	// Pour chaque série on met une option.
												echo("<option value=" . $i . ">" . $series[$i] . "</option>");
											}
										?>
									</select>
							</div>
						</div>
						
						<div class='form-group row'>
							<label for='numero' class='col-sm-4 col-form-labem'>Numéro</label>
							<div class='col-sm-7'>
								<input
									type='number'
									class='form-control'
									id='numero'
									name='numero'
									placeholder='Numéro'>
							</div>
						</div>
						
						<div class='form-group row'>
							<label for='nom' class='col-sm-4 col-form-labem'>Nom</label>
							<div class='col-sm-7'>
								<input
									type='text'
									class='form-control'
									id='nom'
									name='nom'
									placeholder='Nom'
									autofocus='off'>
							</div>
						</div>
						
						<div class='form-group row'>
							<div class='col-sm-11'>
								<button class='btn btn-primary' name='submit' type='submit'>Ajouter</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		
    </body>
</html>
