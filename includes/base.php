<?php

include("config.php");
session_start();

?>

<html lang="fr">
	<?php include("header.php"); ?>
    <body>
		<nav class='navbar navbar-dark navbar-expand-lg fixed-top'>
		<!-- 
		navbar-dark est modifié dans custom.
		navbar-expand-lg permet d'afficher le bouton toggle avec les éléments en colonne sur les écrans trop petits.
		-->
			<a class='navbar-brand' href='/nantflix'>
				<!-- Le logo -->
				<img src='images/logo.png' width='30' height='30' class='d-inline-block align-top' alt='logo Nantflix'>
				Nantflix
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
				<!-- Toggle pour écran trop petit -->
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
				<div class='navbar-nav'>
					<?php
						if (isset($_SESSION['usr'])) { echo(" <!-- Si connecté -->
							<a class='nav-item nav-link' href=/nantflix/series.php>Les séries</a>
							<li class='nav-item dropdown'>
								<a class='nav-link dropdown-toggle' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
									Ajouter
								</a>
								<div class='dropdown-menu' aria-labelledby='navbarDropdown'>
									<a class='dropdown-item' href='/nantflix/nouvelle_serie.php'>Ajouter une série</a>
									<div class='dropdown-divider'></div>
									<a class='dropdown-item' href='/nantflix/nouvel_episode.php'>Ajouter un épisode</a>
									<a class='dropdown-item' href='/nantflix/nouvelle_personne.php'>Ajouter une personne</a>
								</div>
							</li>
							<a class='nav-item nav-link' href=/nantflix/logout.php>Déconnexion</a>"); }
						else { echo(" <!-- Si déconnecté -->
							<a class='nav-item nav-link' href=/nantflix/login.php>Connexion</a>
							<a class='nav-item nav-link' href=/nantflix/register.php>Inscription</a>"); }
						if (isset($navSup)) {
							echo($navSup);
						}
					?>
				</div>
			</div>
        </nav>
