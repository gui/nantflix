<?php
/* A include après avoir include config.php. Affiche un bouton invitant à reprendre au dernier épisode */

	// Dernier épisode joué
	$sql_last_seen = "SELECT ref_episode FROM historique WHERE ref_utilisateur = '{$_SESSION['usr']}' ORDER BY date_visionnage DESC";
	$result_last_seen = mysqli_query($db, $sql_last_seen);
	$row_last_seen = mysqli_fetch_array($result_last_seen, MYSQLI_ASSOC);
	if (isset($row_last_seen['ref_episode'])) {
		$last_seen = $row_last_seen['ref_episode'];

		// Infos sur l'épisode
		$sql_epi = "SELECT ref_serie, numero FROM episode WHERE id={$last_seen}";
		$result_epi = mysqli_query($db, $sql_epi);
		$row_epi = mysqli_fetch_array($result_epi, MYSQLI_ASSOC);
		$serie_id = $row_epi['ref_serie'];
		$last_seen_num = $row_epi['numero'];
		
		// Nom de la série
		$sql_ser = "SELECT nom FROM series WHERE id={$serie_id}";
		$result_ser = mysqli_query($db, $sql_ser);
		$row_ser = mysqli_fetch_array($result_ser, MYSQLI_ASSOC);
		$ser_nom = $row_ser['nom'];
		
		// Tous les épisodes
		$sql_tous = "SELECT * FROM episode WHERE ref_serie={$serie_id} ORDER BY numero";
		$result_tous = mysqli_query($db, $sql_tous);
		if (mysqli_num_rows($result_tous) > 0){
			while($row = mysqli_fetch_array($result_tous, MYSQLI_ASSOC)) {
				if ($row['numero'] > $last_seen_num) {
					$prochain_num = $row['numero'];
					$prochain_nom = $row['nom'];
				break;
				}
			}
		}
		if (isset($prochain_num)) {
			echo(
				"<h2 class='text-center'>Reprenez où vous en étiez : {$ser_nom} épisode {$prochain_num} : {$prochain_nom} </h2><br>
				<div class='row justify-content-center'>
					<a href='/nantflix/includes/redirect.php?dir=/nantflix/play.php?{$serie_id}-{$prochain_num}' class='btn btn-primary btn-lg active' role='button' aria-pressed='true'>Prochain épisode</a>
			</div>
				<br>");
		}
	}
?>
