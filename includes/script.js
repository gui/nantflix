function ajt_zero(nbr) {
    if (nbr < 10) return "0"+nbr;
    else return nbr;
}

function affiche_date(date_sql) {
	// Conversion [A, m, j]
	var t = date_sql.split("-");

	// Création d'une Date. Le problème c'est qu'elle commence en 1970. L'année affichée sera fausse en-dessous.
	var date = new Date(Date.UTC(t[0], t[1]-1, t[2], 0, 0, 0));
	
    // Les jours et mois dans l'ordre anglais
    var jours = new Array("dim", "lun", "mar", "mer", "jeu", "ven", "sam");
    var mois = new Array("janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre");

	if (date.getDate() == 1) var reponse = "1er";
	else var reponse = date.getDate();
    reponse += " "
    reponse += mois[date.getMonth()];
    reponse += " "
    reponse += date.getFullYear();


    document.write(reponse);
}

function ajt_zero(nbr) {
    if (nbr < 10) return "0"+nbr;
    else return nbr;
}

function affiche_date_epi(date_sql) {
	if (date_sql == "jamais") { document.write("jamais"); }
	else {
		var deux = date_sql.split(" ");
		var jour = deux[0].split("-");
		var heure = deux[1].split(":");
		// Création d'une Date. Le problème c'est qu'elle commence en 1970. L'année affichée sera fausse en-dessous.
		var date = new Date(Date.UTC(jour[0], jour[1]-1, jour[2], heure[0]-2, heure[1], heure[2]));
		
		// Les jours et mois dans l'ordre anglais
		var jours = new Array("dim", "lun", "mar", "mer", "jeu", "ven", "sam");
		var mois = new Array("janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre");

		if (date.getDate() == 1) var reponse = "1er";
		else var reponse = date.getDate();
		reponse += " "
		reponse += mois[date.getMonth()];
		reponse += " "
		reponse += date.getFullYear();
		reponse += ", à "
		reponse += ajt_zero(date.getHours());
		reponse += ":"
		reponse += ajt_zero(date.getMinutes());

		document.write(reponse);
	}
}

$(document).ready(function($) {
    $(".table-row").click(function() {
        window.document.location = $(this).data("href");
    });
});

