<?php
/* Page de redirection pour protéger play.php d'une ouverture non redirigée */
// Ceci permet de réactiver $_SESSION['play'] avant de rediriger vers play.php
session_start();
$_SESSION['play'] = true;
$dir = $_GET['dir'];
header('Location: ' . $dir, true);
?>